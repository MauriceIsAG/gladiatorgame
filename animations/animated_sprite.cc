#include "animated_sprite.h"

AnimatedSprite::AnimatedSprite(sf::Time frame_time, bool paused, bool looped) : m_animation(NULL), m_frame_time(frame_time), m_current_frame(0), m_is_paused(paused), m_is_looped(looped), m_texture(NULL)
{

}

void AnimatedSprite::SetAnimation(const Animation &animation)
{
    m_animation = &animation;
    m_texture = m_animation->GetSpriteSheet();
    m_current_frame = 0;
    SetFrame(m_current_frame);
}

void AnimatedSprite::SetFrameTime(sf::Time time)
{
    m_frame_time = time;
}

void AnimatedSprite::Play()
{
    m_is_paused = false;
}

void AnimatedSprite::Play(const Animation &animation)
{
    if (GetAnimation() != &animation)
    {
        SetAnimation(animation);
    }

    Play();
}

void AnimatedSprite::Pause()
{
    m_is_paused = true;
}

void AnimatedSprite::Stop()
{
    m_is_paused = true;
    m_current_frame = 0;
    SetFrame(m_current_frame);
}

void AnimatedSprite::SetLooped(bool looped)
{
    m_is_looped = looped;
}

void AnimatedSprite::SetColor(const sf::Color &color)
{
    m_vertices[0].color = color;
    m_vertices[1].color = color;
    m_vertices[2].color = color;
    m_vertices[3].color = color;
}

const Animation* AnimatedSprite::GetAnimation() const
{
    return m_animation;
}

sf::FloatRect AnimatedSprite::GetLocalBounds() const
{
    sf::IntRect rectangle = m_animation->GetFrame(m_current_frame);

    float width = static_cast<float>(std::abs(rectangle.width));
    float height = static_cast<float>(std::abs(rectangle.height));

    return sf::FloatRect(0.f, 0.f, width, height);
}

sf::FloatRect AnimatedSprite::GetGlobalBounds() const
{
    return getTransform().transformRect(GetLocalBounds());
}

bool AnimatedSprite::IsLooped() const
{
    return m_is_looped;
}

bool AnimatedSprite::IsPlaying() const
{
    return !m_is_paused;
}

sf::Time AnimatedSprite::GetFrameTime() const
{
    return m_frame_time;
}

void AnimatedSprite::SetFrame(std::size_t new_frame, bool reset_time)
{
    if (m_animation)
    {
        sf::IntRect rectangle = m_animation->GetFrame(new_frame);

        m_vertices[0].position = sf::Vector2f(0.f, 0.f);
        m_vertices[1].position = sf::Vector2f(0.f, static_cast<float>(rectangle.height));
        m_vertices[2].position = sf::Vector2f(static_cast<float>(rectangle.width), static_cast<float>(rectangle.height));
        m_vertices[3].position = sf::Vector2f(static_cast<float>(rectangle.width), 0.f);

        float left = static_cast<float>(rectangle.left) + 0.0001f;
        float right = left + static_cast<float>(rectangle.width);
        float top = static_cast<float>(rectangle.top);
        float bottom = top + static_cast<float>(rectangle.height);

        m_vertices[0].texCoords = sf::Vector2f(left, top);
        m_vertices[1].texCoords = sf::Vector2f(left, bottom);
        m_vertices[2].texCoords = sf::Vector2f(right, bottom);
        m_vertices[3].texCoords = sf::Vector2f(right, top);
    }
    if (reset_time)
    {
        m_current_time = sf::Time::Zero;
    }
}

void AnimatedSprite::Update(sf::Time delta_time)
{
    if (!m_is_paused && m_animation)
    {
        m_current_time += delta_time;

        if (m_current_time >= m_frame_time)
        {
            m_current_time = sf::microseconds(m_current_time.asMicroseconds() % m_frame_time.asMicroseconds());

            if (m_current_frame + 1 < m_animation->GetSize())
            {
                m_current_frame++;
            }
            else
            {
                m_current_frame = 0;

                if (!m_is_looped)
                {
                    m_is_paused = true;
                }
            }
            SetFrame(m_current_frame, false);
        }
    }
}

void AnimatedSprite::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    if (m_animation && m_texture)
    {
        states.transform *= getTransform();
        states.texture = m_texture;
        target.draw(m_vertices, 4, sf::Quads, states);
    }
}
