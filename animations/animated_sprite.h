#ifndef ANIMATED_SPRITE_H
#define ANIMATED_SPRITE_H

#include "animation.h"
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

class AnimatedSprite : public sf::Drawable, public sf::Transformable
{
private:
    const Animation* m_animation;
    sf::Time m_frame_time;
    sf::Time m_current_time;
    std::size_t m_current_frame;
    bool m_is_paused;
    bool m_is_looped;
    const sf::Texture* m_texture;
    sf::Vertex m_vertices[4];

    virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;

public:
    explicit AnimatedSprite(sf::Time frame_time = sf::seconds(0.2f), bool paused = false, bool looped = true);

    void Update(sf::Time delta_time);
    void SetAnimation(const Animation &animation);
    void SetFrameTime(sf::Time time);
    void Play();
    void Play(const Animation &animation);
    void Pause();
    void Stop();
    void SetLooped(bool looped);
    void SetColor(const sf::Color &color);
    const Animation* GetAnimation() const;
    sf::FloatRect GetLocalBounds() const;
    sf::FloatRect GetGlobalBounds() const;
    bool IsLooped() const;
    bool IsPlaying() const;
    sf::Time GetFrameTime() const;
    void SetFrame(std::size_t new_frame, bool reset_time = true);
};

#endif // ANIMATED_SPRITE_H
