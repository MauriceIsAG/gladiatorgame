#include "animation.h"

void Animation::AddFrame(sf::IntRect rectangle)
{
    m_frames.push_back(rectangle);
}

void Animation::SetSpriteSheet(const sf::Texture &texture)
{
    m_texture = &texture;
}

const sf::Texture* Animation::GetSpriteSheet() const
{
    return m_texture;
}

std::size_t Animation::GetSize() const
{
    return m_frames.size();
}

const sf::IntRect& Animation::GetFrame(std::size_t index) const
{
    return m_frames[index];
}
