#ifndef ANIMATION_H
#define ANIMATION_H

#include <SFML/Graphics.hpp>
#include <vector>

class Animation
{
private:
    std::vector<sf::IntRect> m_frames;
    const sf::Texture* m_texture;

public:
    Animation() : m_texture(nullptr) {}
    void AddFrame(sf::IntRect rectangle);
    void SetSpriteSheet(const sf::Texture &texture);
    const sf::Texture* GetSpriteSheet() const;
    std::size_t GetSize() const;
    const sf::IntRect& GetFrame(std::size_t index) const;
};

#endif // ANIMATION_H
