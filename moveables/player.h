#ifndef PLAYER_H
#define PLAYER_H

#include "../animations/animated_sprite.h"
#include "../animations/animation.h"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <iostream>

class Player
{
private:
    sf::Vector2i m_screen_size;
    sf::Texture m_texture;
    Animation m_walking_down;
    Animation m_walking_left;
    Animation m_walking_right;
    Animation m_walking_up;
    AnimatedSprite m_animated_sprite;

public:
    Player(std::string texture = "../sprite_sheets/test1.png", int screen_x = 800, int screen_y = 800);

};

#endif // PLAYER_H
